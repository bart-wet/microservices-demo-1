package nl.capgemini.wet.orders.model;

public class MenuOrder {

    private String burger;
    private String drink;

    public void setBurger(final String burger) {
        this.burger = burger;
    }

    public void setDrink(final String drink) {
        this.drink = drink;
    }

    public String getBurger() {
        return burger;
    }

    public String getDrink() {
        return drink;
    }

}
