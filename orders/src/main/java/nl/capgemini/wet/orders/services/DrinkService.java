package nl.capgemini.wet.orders.services;

public interface DrinkService {

    String requestDrink(String order);

}
