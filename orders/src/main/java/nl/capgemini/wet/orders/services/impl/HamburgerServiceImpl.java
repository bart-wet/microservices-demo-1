package nl.capgemini.wet.orders.services.impl;

import nl.capgemini.wet.orders.services.HamburgerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class HamburgerServiceImpl implements HamburgerService {

    private final RestTemplate restTemplate;
    private final Environment environment;
    private static final Logger log = LoggerFactory.getLogger(HamburgerServiceImpl.class);

    public HamburgerServiceImpl(final RestTemplate restTemplate, final Environment environment) {
        this.restTemplate = restTemplate;
        this.environment = environment;
    }

    @Override
    public String requestBurger(final String order) {
        log.info("Sending hamburger request");
        try {
            final ResponseEntity<String> response = restTemplate.postForEntity(environment.getRequiredProperty("hamburger.url"), order, String.class);
            if (response.getStatusCode() == HttpStatus.OK) {
                log.info("Hamburger delivered");
                return response.getBody();
            } else {
                log.error("Hamburger order failed: Error {}: {}", response.getStatusCode(), response.getBody());
            }
        } catch (final Exception e) {
            log.error("Hamburger order failed", e);
        }
        return "No hamburger delivered";
    }

}
