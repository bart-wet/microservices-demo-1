package nl.capgemini.wet.orders.services.impl;

import nl.capgemini.wet.orders.services.DrinkService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class DrinkServiceImpl implements DrinkService {

    private static final Logger log = LoggerFactory.getLogger(DrinkServiceImpl.class);
    private final RestTemplate restTemplate;
    private final Environment environment;

    public DrinkServiceImpl(final RestTemplate restTemplate, final Environment environment) {
        this.restTemplate = restTemplate;
        this.environment = environment;
    }

    @Override
    public String requestDrink(final String order) {
        log.info("Sending drink request");
        try {
            final ResponseEntity<String> response = restTemplate.postForEntity(environment.getRequiredProperty("drink.url"), order, String.class);
            if (response.getStatusCode() == HttpStatus.OK) {
                log.info("Drink delivered");
                return response.getBody();
            } else {
                log.error("Drink order failed: Error {}: {}", response.getStatusCode(), response.getBody());
            }
        } catch (final Exception e) {
            log.error("Drink order failed", e);
        }
        return "No drink delivered";
    }

}
