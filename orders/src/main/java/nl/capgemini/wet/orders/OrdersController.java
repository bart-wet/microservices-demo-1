package nl.capgemini.wet.orders;

import nl.capgemini.wet.orders.model.MenuOrder;
import nl.capgemini.wet.orders.services.DrinkService;
import nl.capgemini.wet.orders.services.HamburgerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

@Controller
public class OrdersController {

    private final HamburgerService hamburgerService;
    private final DrinkService drinkService;
    private static final Logger log = LoggerFactory.getLogger(OrdersController.class);

    public OrdersController(final HamburgerService hamburgerService, final DrinkService drinkService) {
        this.hamburgerService = hamburgerService;
        this.drinkService = drinkService;
    }

    @GetMapping
    public String index() {
        return "index";
    }

    @PostMapping("/orders/new")
    @ResponseBody
    public ResponseEntity<Map<String, String>> order(final @RequestBody MenuOrder order) {
        log.info("recieved order");
        try {
            final HashMap<String, String> result = new HashMap<>();
            result.put("hamburger", hamburgerService.requestBurger(order.getBurger()));
            result.put("drink", drinkService.requestDrink(order.getDrink()));
            return ResponseEntity.ok(result);
        } finally {
            log.info("Sent order response");
        }
    }

}
