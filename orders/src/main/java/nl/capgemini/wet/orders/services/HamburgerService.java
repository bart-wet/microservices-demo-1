package nl.capgemini.wet.orders.services;

public interface HamburgerService {

    String requestBurger(String order);

}
