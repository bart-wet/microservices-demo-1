<!DOCTYPE html>
<html>
<head>
    <title>Capgemini's Bar</title>
    <script type="text/javascript">
        function submitform(form) {
            let xhr = new XMLHttpRequest();
            xhr.open(form.method, form.action, true);
            xhr.setRequestHeader('Content-Type', 'application/json');
            xhr.onreadystatechange = function () {
                if (xhr.readyState === XMLHttpRequest.DONE) {
                    let status = xhr.status;
                    if (status === 0 || (status >= 200 && status < 400)) {
                        // The request has been completed successfully
                        alert(xhr.responseText);
                    } else {
                        alert("Error (" + status + "): " + xhr.responseText);
                    }
                }
            };
            let order = {
                "burger": form.querySelector('select[name="hamburger"]').value,
                "drink": form.querySelector('select[name="drink"]').value
            }
            xhr.send(JSON.stringify(order));
        }
    </script>
</head>
<body style="background-color: black;color: white">
<form method="post" action="/orders/new">
    <select name="hamburger">
        <option value="simple-hamburger">Simple Hamburger</option>
        <option value="cheeseburger">Cheeseburger</option>
    </select>
    <select name="drink">
        <option value="cola">Cola</option>
        <option value="water">Water</option>
    </select>
    <button type="button" onclick="submitform(this.parentElement)">Place order</button>
</form>
</body>
</html>
