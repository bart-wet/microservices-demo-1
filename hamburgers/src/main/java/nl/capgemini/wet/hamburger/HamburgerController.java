package nl.capgemini.wet.hamburger;

import nl.capgemini.wet.hamburger.services.HamburgerService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HamburgerController {

    private final HamburgerService hamburgerService;

    public HamburgerController(final HamburgerService hamburgerService) {
        this.hamburgerService = hamburgerService;
    }

    @PostMapping("/hamburger/new")
    public ResponseEntity<String> order(final @RequestBody String order) {
        final String result = hamburgerService.requestBurger(order);
        return ResponseEntity.ok(result);
    }

}
