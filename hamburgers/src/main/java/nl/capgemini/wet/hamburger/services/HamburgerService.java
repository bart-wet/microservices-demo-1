package nl.capgemini.wet.hamburger.services;

public interface HamburgerService {

    String requestBurger(String order);

}
