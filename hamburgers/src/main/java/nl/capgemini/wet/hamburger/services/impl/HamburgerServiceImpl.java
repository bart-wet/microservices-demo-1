package nl.capgemini.wet.hamburger.services.impl;

import nl.capgemini.wet.hamburger.services.HamburgerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class HamburgerServiceImpl implements HamburgerService {

    private static final Logger log = LoggerFactory.getLogger(HamburgerServiceImpl.class);

    @Override
    public String requestBurger(final String order) {
        log.info("Creating hamburger");
        return order + " delivered";
    }

}
