# Microservices Demo 1

A simple demonstation of a microservice architecture. This example is based on a hamburger menu ordering system, in
which each part of the menu is "created" by a separate microservice.

This demo is meant to show how a synchronous microservice landscape can be built using Spring Boot. It doesn't include
full business functionality and isn't meant to show that either. As such the site is kept simple and straight forward.

## Structure 

There are 2 implemented microservices: 
- Orders: the main ordering microservice
- Hamburgers: the hamburger "creation" service

## Orders

This microservice forms the front-end controller of the site. It can be started via the main class 
"nl.capgemini.wet.orders.OrdersApplication". After that one can post an order in the browser via the following address:
"http://localhost:8080".

## Hamburgers

This microservice "creates" and "delivers" a hamburger to the browser. It can be started via the main class 
"nl.capgemini.wet.hamburger.HamburgerApplication". This delivery runs through the Orders service to the browser.

# Future steps for this example

- Create the drinks Microservice
- Expand with the sidedish Microservice
- Service classes in Orders Microservice can be made more DRY

# Options to explore

- Change into asynchronous with message queues
- Asynchronous HTTP calls
- Docker(-compose)/kubernetes infrastructure 